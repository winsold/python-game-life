import pygame
import re

# TODO try to print at the top of the window game
# "press backspace and you will see the life after N generations"

pygame.display.set_caption('The game of life')
f = open('game field.txt')
line = f.readline()
width = (len(line)-1) * 50
f.close()
f = open('game field.txt')
height = (len(re.findall(r"[\n']+", f.read()))) * 50
f.close()
screen = pygame.display.set_mode((height, width))


class GameOfLife:
    def __init__(self, cell_size=50, speed=0.01):
        self.cell_size = cell_size
        self.cell_width = width // self.cell_size
        self.cell_height = height // self.cell_size
        self.speed = speed

    def universe(self):
        # import numpy as np
        # self.cell_list = np.random.randint(0, 4,
        # (height//self.cell_size, width//self.cell_size))
        self.cell_list = []
        with open('game field.txt') as fin:
            for line in fin:
                self.cell_list.append(list(line.rstrip('\n')))
        # print(self.cell_list)
        self.cell_tuple = list(self.cell_list)
        for i in range(len(self.cell_list)):
            self.cell_tuple[i] = tuple(self.cell_list[i])
        self.cell_tuple = tuple(self.cell_tuple)
        # print(self.cell_tuple)

    def cell_list_draw(self):
        for i in range(0, height//self.cell_size):
            for j in range(0, width//self.cell_size):
                if self.cell_tuple[i][j] == '1':
                    self.fish = pygame.image.load('fish.png')
                    screen.blit \
                        (self.fish, (i * self.cell_size, j * self.cell_size))
                elif self.cell_tuple[i][j] == '2':
                    self.shrimp = pygame.image.load('shrimp.png')
                    screen.blit \
                        (self.shrimp, (i * self.cell_size, j * self.cell_size))
                if self.cell_tuple[i][j] == '3':
                    self.rock = pygame.image.load('rock.png')
                    screen.blit \
                        (self.rock, (i * self.cell_size, j * self.cell_size))
                if self.cell_tuple[i][j] == '0':
                    self.sea = pygame.image.load('sea.png')
                    screen.blit \
                        (self.sea, (i * self.cell_size, j * self.cell_size))

    def state_def(self):
        s = 0
        p = 0
        # import numpy as np
        # import copy
        # new_universe = copy.copy(cell_list)
        self.new_universe = list(self.cell_tuple)
        for i in range(len(self.cell_tuple)):
            self.new_universe[i] = list(self.cell_tuple[i])
        for x in range(height // self.cell_size):
            for y in range(width // self.cell_size):
                for i in range(-1, 2):
                    for j in range(-1, 2):
                        if not (i == j == 0) and \
                            (i + x <= height//self.cell_size - 1) and \
                                (j + y <= width//self.cell_size - 1) \
                                and (i + x >= 0) and (j + y >= 0):
                            if self.cell_tuple[x][y] == '1':
                                if self.cell_tuple[x+i][y+j] == '1':
                                    s += 1
                            elif self.cell_tuple[x][y] == '2':
                                if self.cell_tuple[x + i][y + j] == '2':
                                    p += 1
                            elif self.cell_tuple[x][y] == '0':
                                if self.cell_tuple[x + i][y + j] == '1':
                                    s += 1
                                elif self.cell_tuple[x + i][y + j] == '2':
                                    p += 1
                if self.cell_tuple[x][y] == '1':
                    if (s >= 4) or (s < 2):
                        self.new_universe[x][y] = '0'
                if self.cell_tuple[x][y] == '2':
                    if (p >= 4) or (p < 2):
                        self.new_universe[x][y] = '0'
                if self.cell_tuple[x][y] == '0':
                    if s > 2:
                        self.new_universe[x][y] = '1'
                    elif p > 2:
                        self.new_universe[x][y] = '2'
                s = 0
                p = 0
        # cell_list = copy.copy(new_universe)
        self.cell_tuple = self.new_universe
        for i in range(len(self.cell_tuple)):
            self.cell_tuple[i] = tuple(self.new_universe[i])
        self.cell_tuple = tuple(self.cell_tuple)

    def draw_screen(self):
        for x in range(0, width, self.cell_size):
            pygame.draw.line(screen, pygame.Color('blue'),
                             (x, 0), (x, height))
        for y in range(0, height, self.cell_size):
            pygame.draw.line(screen, pygame.Color('blue'),
                             (0, y), (width, y))

    def run(self):
        pygame.init()
        pygame.display.set_caption('Game of Life')
        screen.fill(pygame.Color('blue'))
        running = True
        self.drawScreen()
        self.universe()
        self.cellListDraw()
        file = open('generations.txt')
        generations = int(str(file.readline()))
        while running:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_BACKSPACE:
                        for i in range(0, generations):
                            self.state_def()
                            self.cellListDraw()
                if event.type == pygame.QUIT:
                    running = False
            pygame.display.flip()
        file.close()
        pygame.quit()
if __name__ == '__main__':
    game = GameOfLife(50)
    game.run()
